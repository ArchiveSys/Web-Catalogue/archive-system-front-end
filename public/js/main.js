import { CONFIG } from './config.js';
import { Catalog_System } from './catalouge/Catalog_System.js';

let catalouge = new Catalog_System(CONFIG.API_ENTRYPOINT, CONFIG.HTTP_ENTRYPOINT);
catalouge.refresh();
window.Archive = {'catalouge':catalouge};

console.info("All modules loaded.");
