/* esversion: 6 */

/**
 * Extention for working with events.
 * @extends {toolkit}
 * @todo re-work this since it should save all set listeners for removal.
 */
export class eventToolkitExtention {

  /**
   * Sets up the members for this extention.
   * @public
   */
  initializer(){
    this.addExtention("eventToolkitExtention");
    this.listening = {};
  }

  /**
   * Adds A Listener.
   * @public
   * @param {String} eventType - What type of event are we looking for?
   * @param {function} funcToDo - The function to run when the event is triggered.
   */
  addListener(eventType, funcToDo){
    this.hasElement();
    this.doEach( (element, index) => { element.addEventListener(eventType, funcToDo); });
    if(this.listening[eventType] == undefined){
      this.listening[eventType] = [];
    }
    this.listening[eventType].push(funcToDo);
  }

  /**
   * Removes A specified Listener.
   * @public
   * @param {String} eventType - What type of event are we looking for?
   * @param {function} funcToNotDo - The function to no longer run when the event is triggered.
   */
  removeListener(eventType, funcToNotDo){
    this.hasElement();
    this.doEach( (element, index) => { element.removeEventListener(eventType, funcToNotDo); });
    if(this.listening[eventType]){
      let index = this.listening[eventType].indexOf(funcToNotDo);
      if (index >= 0) {
        this.listening[eventType].splice( index, 1 );
      }
    }
  }

  /**
   * Removes All Listeners for a event type.
   * @public
   * @param {String} eventType - The event type to remove all event listeners from.
   */
  removeAllListeners(eventType){
    this.hasElement();
    let tkSelf = this;
    this.listening[eventType].forEach(function(listenerToRemove, index){
        tkSelf.doEach( function (element, index) {
          element.removeEventListener(eventType, listenerToRemove);
        });
    });
    this.listening[eventType] = [];
  }
};
