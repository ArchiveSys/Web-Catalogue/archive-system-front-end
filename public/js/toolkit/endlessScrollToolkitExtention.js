/* esversion: 6 */

/**
 * Extention for messing with the text and raw HTML content of a webpage.
 * @extends {toolkit}
 */
export class endlessScrollToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * @public
   * @ignore
   */
  initializer(){
    /**
     * Handles infininte scroll data storage.
     * @private
     */
    this.infininteScroll = {};
    this.addExtention("endlessScrollToolkitExtention");
  }

  /**
   * Checks if a element is in the viewport.
   * @private
   * @param {Element} element - The element to check.
   * @returns {boolean} if the element was in the viewport.
   */
  __isElementInViewport (element) {
     let rect = element.getBoundingClientRect();

     let visibility = (
       rect.top >= 0 &&
       rect.left >= 0 &&
       rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
       rect.right <= (window.innerWidth || document.documentElement.clientWidth)
     )

     return visibility;
   }

  /**
   * Checks if the element changed status. (Was in VP but not not / Was not but now is)
   * @private
   */
   __onVisibilityChange() {
   this.infininteScroll.oldVisible = false;
   let self = this;
   return function () {
     if(self.infininteScroll.element){
       let visible = self.__isElementInViewport(self.infininteScroll.element);
       if (visible != self.infininteScroll.oldVisible) {
         self.infininteScroll.oldVisible = visible;
         if (typeof self.infininteScroll.callback == 'function') {
           self.infininteScroll.callback(visible);
         }
       }
     }

   }
  }

  /**
   * Adds the listeners for the infininte scroll.
   * @private
   * @param {function} listener - the function to call back to.
   */
  __InfininteScrollAddListeners(listener) {
    // these are global stuff, don't use the event toolkit for these.
    addEventListener('DOMContentLoaded', listener, false);
    addEventListener('load', listener, false);
    addEventListener('scroll', listener, false);
    addEventListener('resize', listener, false);
  }

  /**
   * Adds the infininte scroll callback.
   * @public
   * @param {function} callback - the function to call back to. Will get passed the visibility whenever called.
   */
  addInfininteScroll(callback){
    this.infininteScroll.callback = callback
  }

  /**
   * Set's which element should trigger the infininte scroll event.
   * TOOLKIT MUST ONLY HAVE ONE ITEM SELECTED!
   * @public
   */
  enableInfininteScroll(){
    this.hasElement();
    if(this.elements.length > 1){
      throw this.ERRORS.INVALID_ELEMENT_COUNT;
    }
    this.infininteScroll.element = this.elements[0];
    this.infininteScroll.handeler = this.__onVisibilityChange();
    this.__InfininteScrollAddListeners(this.infininteScroll.handeler);
  }

  /**
   * Disables the infininte scroll event.
   * @public
   */
  disableInfininteScroll(){
    this.infininteScroll.element = false;
  }
};
