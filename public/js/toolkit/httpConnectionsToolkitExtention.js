/* esversion: 6 */

/**
 * Extention for obtaining data useing HTTP.
 * @extends {toolkit}
 */
export class httpConnectionsToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * @public
   * @ignore
   */
  initializer(){
    this.addExtention("httpConnectionsToolkitExtention");
  }

  /**
   * Makes a get connection.
   * @public
   * @param {String} page - The page want to hit with the post request.
   * @param {Object} dataIn - Object containing key-value pairs to be sent in the post request.
   * @param {function} funcToRun - the callback function.
   * @emits callback(Object) object callback with the server's response.
   * @todo learn Promise and update code.
   */
  get(page, dataIn, funcToRun){
    let thisRequest = new XMLHttpRequest();
    thisRequest.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        funcToRun(this.responseText);
      }
    };
    thisRequest.open("GET", (page + "?" + this.__urlEncoder(dataIn)), true);
    thisRequest.send();
  }

  /**
   * Makes a post connection.
   * @public
   * @param {String} page - The page want to hit with the post request.
   * @param {Object} dataIn - Object containing key-value pairs to be sent in the post request.
   * @param {function} funcToRun - the callback function.
   * @emits callback(Object) object callback with the server's response.
   * @todo learn Promise and update code.
   */
  post(page, dataIn, funcToRun){
    let thisRequest = new XMLHttpRequest();
    thisRequest.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        funcToRun(this.responseText);
      }
    };
    thisRequest.open("POST", page, true);
    thisRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    thisRequest.send(this.__urlEncoder(dataIn));
  }

  /**
   * Encodes a object useing URL encodeing (For querys)
   * Taken from stackoverflow
   * @private
   * @see https://stackoverflow.com/questions/1714786/query-string-encoding-of-a-javascript-object
   * @param {Object} obj - Object to be encoded for transfer.
   * @param {String|*} [prefix] - the prefix for items?
   */
  __urlEncoder(obj, prefix){
    let str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        let k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        if(v !== undefined){
          str.push((v !== null && typeof v === "object") ?
            serialize(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
      }
    }
    // REMINDER: THIS IS FREKIN USEFULL!
    return str.join("&");
  }
};
