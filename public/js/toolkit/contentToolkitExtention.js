/* esversion: 6 */

/**
 * Extention for messing with the text and raw HTML content of a webpage.
 * @extends {toolkit}
 */
export class contentToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * @public
   * @ignore
   */
  initializer(){
    this.addExtention("contentToolkitExtention");
  }

  /**
   * Replaces text of selected element(s).
   * Escapes HTML.
   * @public
   * @param {String} text - Text (that will be escaped) to replace current text.
   */
  replaceText(text) {
    this.hasElement();
    this.doEach((element, index) => { element.textContent = text; });
  }

  /**
   * Replaces inner HTML of selected element(s).
   * Does not escape HTML.
   * @public
   * @param {DOMString} html - HTML that will replace current inner HTML
   */
  replaceHTML(html) {
    this.hasElement();
    this.doEach((element, index) => { element.innerHTML = html; });
  }

  /**
   * Appends to the inner HTML of selected element(s).
   * Does not escape DOMString. Maintains Event Handelers.
   * @public
   * @param {Node} html - Node HTML that will replace current inner HTML.
   * @example
   * dom == "<div id='parrent'></div>";
   * let html = "<p id='child'>some text</p>";
   * let wrapper = document.createElement('div');
   * wrapper.id("childWrapper");
   * wrapper.innerHTML = html;
   * tk("#parrent").appendHTML(wrapper);
   * dom == "<div id='parrent'><div id='childWrapper'><p id='child'>sometext</p></div>/div>";
   */
  appendHTML(html) {
    this.hasElement();
    this.doEach( (element, index) => { element.append(html); });
  }
};
