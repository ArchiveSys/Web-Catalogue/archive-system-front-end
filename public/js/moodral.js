/**
 * A minimilistic moodral class that can handle displaying and hideing moodrals.
 *
 * Designed so multiple moodrals can use the same background without messing everything up.
 */
 export default class moodral {
   /**
    * Get's the moodral and it's background element.
    * @public
    * @param {string} moodralID - the ID of the moodral element. (should have a opaque, full-page element as a parrent)
    */
  constructor(moodralID){
    /**
     * The moodral node.
     * @type {Node}
     */
    this.moodral = document.getElementById(moodralID);
    /**
     * The moodral background node.
     * @type {Node}
     */
    this.moodralBG = this.moodral.parentElement;
  }

  /**
   * Show's the moodral.
   * @public
   */
  show(){
    if(this.moodralBG.dataset["using"] == undefined){
      this.moodralBG.dataset["using"] = 0;
    }
    this.moodralBG.dataset["using"]++;
    this.moodralBG.style["display"] = "block";
    this.moodral.style["display"] = "block";
  }

  /**
   * Hides the moodral.
   * @public
   */
  hide(){
    this.moodral.style["display"] = null;
    this.moodralBG.dataset["using"]--;
    if(this.moodralBG.dataset["using"] <= 0){
      this.moodralBG.style["display"] = null;
      this.moodralBG.dataset["using"] = 0;
    }
  }

  /**
   * Adds a close button to the moodral.
   * @public
   * @param {String} closerSelector - a CSS selector of the closer element WITHIN the moodral.
   */
  addCloseButton(closerSelector){
    let closer = this.moodral.querySelector(closerSelector);
    closer.addEventListener("click", (e) => {this.hide();}, false);
  }
}
