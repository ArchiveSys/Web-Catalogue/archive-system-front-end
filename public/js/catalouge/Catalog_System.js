import { aggregator, toolkit } from './../toolkit/baseToolkit.js';
import {contentToolkitExtention} from './../toolkit/contentToolkitExtention.js';

class tk extends aggregator(toolkit, contentToolkitExtention) {};

import { Catalog_System_Base } from './Catalog_System_Base.js';
import { Filter } from './Filter/Filter.js';
import { Page_Headers } from './Page_Headers/Page_Headers.js';

/**
 * Handles displaying the catalog.
 * @extends {Catalog_System_Base}
 */
export class Catalog_System extends Catalog_System_Base {
  /**
   * The constructor for the class.
   * @public
   * @param {String} API_Entrypoint - Where to send API requests.
   * @param {String} HTTP_Entrypoint - Where to get HTML data from.
   */
  constructor(API_Entrypoint, HTTP_Entrypoint){
    super(API_Entrypoint, HTTP_Entrypoint);
    /**
     * A toolkit for accessing the list itself.
     *
     * Used in Catalog_Entry.addHTML() passed through Catalog_Entry.addEntry() and in clearEntries.
     * @private
     * @type {Toolkit}
     */
    this.list = new tk("#BookList");
    /**
     * The filter object. Handles the search/filter menu.
     * @private
     * @type {Filter_Base}
     * @todo replace the current object
     */
    this.filter = new Filter(this);
    /**
     * Adds page headers / footers and infininte scroll functionality.
     * @public
     * @type {Page_Headers}
     */
    this.pageHeaders = new Page_Headers(this);
  }

  /**
   * Removes all currently displayed entries.
   * @public
   */
  clearEntries(){
    super.clearEntries();
    this.list.replaceHTML("");
  }

  /**
   * Full Refresh of the catalouge.
   * Call this when changeing the search options.
   * @public
   */
  reloadFilter(){
    this.pageHeaders.lockoutInfininteScroll();
    window.scrollTo(0, 0);
    super.reloadFilter();
    // add loading spinner
  }

  /**
   * Called when the catalog data is returned.
   * @private
   * @param {Object} catalogList - A page of catalog entries to display on page one.
   */
  __prossessAPage(catalogList){
    let localPage = super.__prossessAPage(catalogList);
    let self = this;
    localPage.forEach((thisEntry) => {thisEntry.addEntry(self.list);});
    this.pageHeaders.waitingNextPage(self.list);
  }
}
