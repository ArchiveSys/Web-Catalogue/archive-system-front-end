/**
 * Handles connecting to the archive server for catalouge searches.
 *
 * Designed so that when the back-end is overhauled this can be easaly replaced.
 */
export class Archive_Connection {
  constructor(API_Entrypoint, catalog) {
    /**
     * The API Entrypoint; The base URL that is before all other requests.
     * @type {String}
     */
    this.API_Entrypoint = API_Entrypoint;
    /**
     * The object that provides what filters are currently selected.
     * @type {Catalog_System}
     */
    this.catalog = catalog;
  }

  changeEndpoint(NEW_API_Entrypoint){
    this.API_Entrypoint = NEW_API_Entrypoint;
  }

  /**
   * Preps the JSON data and sends it to the loadEntrys function and, if on the first page, the loadOptions function.
   *
   * This should be phased out in the near future.
   * @public
   * @deprecated
   */
  loadEntryAndOptions(){
    let self = this;
    let options = self.catalog.filter.getPayload();
    let optionsJSON = JSON.stringify(options);
    this.loadEntrys(optionsJSON);
    if(self.catalog.filter.page == 1){
      this.loadOptions(optionsJSON);
    }
  }

  /**
   * Loads a page of catalog entrys. When they are obtained it will send it back to the catalouge.
   * @public
   * @param {JSONString} optionsJSON - the options to send to the server.
   */
  loadEntrys(optionsJSON){
    let self = this;
    fetch((this.API_Entrypoint + "catalouge"), {"method":"POST", "body":optionsJSON}).then(res => res.json()).then(data => {
        self.catalog.__prossessAPage(data);
    });
  }

  /**
   * Loads the currently available filter options. When they are obtained it will send it back to the filter object.
   * @public
   * @param {JSONString} optionsJSON - the options to send to the server.
   */
  loadOptions(optionsJSON){
    let self = this;
    fetch((this.API_Entrypoint + "tags"), {"method":"POST", "body":optionsJSON}).then(res => res.json()).then(data => {
      self.catalog.filter.updateOptions(data);
    })
  }
}
