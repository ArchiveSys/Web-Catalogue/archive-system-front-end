import { Catalog_Entry } from './Catalog_Entry/Catalog_Entry.js';
import { Archive_Connection } from './Archive_Connection/Archive_Connection.js';


/**
 * The actual catalog object, splits the data between the various sections of the code.
 */
export class Catalog_System_Base {
  /**
   * The constructor for the class.
   * @public
   * @param {String} API_Entrypoint - Where to send API requests.
   * @param {String} HTTP_Entrypoint - Where to get HTML data from.
   */
  constructor(API_Entrypoint, HTTP_Entrypoint){
    /**
     * Where to get HTML content from.
     * @private
     * @type {String}
     */
    this.HTTP_Entrypoint = HTTP_Entrypoint;
    /**
     * The filter object. Handles the search/filter menu.
     * @private
     * @type {Filter_Base}
     * @abstract
     * @todo replace the current object
     */
    this.filter = undefined;
    /**
     * All currently displayed entries.
     * @private
     * @type {Array<Catalog_System_Base>}
     */
    this.entries = [];
    /**
     * Every page with every entries that is on said page each page.
     * @private
     * @type {Array<Array<Catalog_System_Base>>}
     */
    this.pages = [];
    /**
     * The connection to the archive.
     * @private
     * @type {Archive_Connection}
     */
    this.connection = new Archive_Connection(API_Entrypoint, this);
  }

  /**
   * Changes the entrypoint, mostly used for switching to a dev API instance.
   *
   * @param {URL} HTTP_Entrypoint
   * @param {URL} API_Entrypoint
   */
  changeEndpoint(HTTP_Entrypoint, API_Entrypoint){
    if(HTTP_Entrypoint){
      this.HTTP_Entrypoint = HTTP_Entrypoint;
    }
    if(API_Entrypoint){
      this.connection.changeEndpoint(API_Entrypoint);
    }
  }

  /**
   * Clears the currently saved entries and the list of currently saved entries.
   * @public
   */
  clearEntries(){
    this.entries = [];
    this.pages = [];
    this.filter.setPage(1);
  }

  /**
   * Full reload with the new filter parameters.
   * Call this when changeing the search filters.
   * @public
   */
  reloadFilter(){
    this.clearEntries();
    let self = this;
    this.connection.loadEntryAndOptions();
  }

  /**
   * Starts the load of the next page for the catalog.
   * @public
   */
  nextPage(){
    // add loading spinner
    let self = this;
    this.connection.loadEntryAndOptions();
  }

  /**
   * Called when the catalog data is returned.
   * @private
   * @param {Object} catalogList - A page of catalog entries to display on page one.
   */
  __prossessAPage(catalogList){
    let self = this;
    let localPage = [];
    catalogList.forEach((data, index) => {self.__addEntry(data, localPage);});
    this.pages.push(localPage);
    return localPage;
  }

  /**
   * Adds a entry to the catalouge.
   * @private
   * @param {Object} entryData - The Individual entry's data
   * @param {number} page - the page that this item should be added to.
   */
  __addEntry(entryData, page){
    let thisEntry = new Catalog_Entry(entryData, this, this.HTTP_Entrypoint);
    this.entries.push(thisEntry);
    page.push(thisEntry);
  }
}
