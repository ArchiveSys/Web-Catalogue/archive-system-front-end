import { aggregator, toolkit } from './../../toolkit/baseToolkit.js';
import {contentToolkitExtention} from './../../toolkit/contentToolkitExtention.js';
import {styleToolkitExtention} from './../../toolkit/styleToolkitExtention.js';
import {endlessScrollToolkitExtention} from './../../toolkit/endlessScrollToolkitExtention.js';

class tk extends aggregator(toolkit, contentToolkitExtention, styleToolkitExtention, endlessScrollToolkitExtention) {};

/**
 * This implements the infininte scroll functionality and the top / bottom of the infininte pages.
 */
export class Page_Headers {
  constructor(catalog){
    /**
     * The parrent catalog.
     * @public
     * @type {Catalog_System}
     */
    this.catalog = catalog;

    let self = this;

    /**
     * The infininte scroll toolkit.
     * @private
     * @type {toolkit}
     */
    this.ifHandler = new tk();
    this.ifHandler.addInfininteScroll((x) => {self.onInfininteScroll(x);});
  }

  /**
   * Creates the next page div that triggers infininte scroll functionality. (Does not add)
   * @private
   * @returns {Node}
   */
  __createNextPageDiv(){
    let endlessScrollDiv = document.createElement('div');
    endlessScrollDiv.id = "EndlessScroll";
    endlessScrollDiv.classList.add('z-list');
    endlessScrollDiv.classList.add('z-hidden');
    endlessScrollDiv.classList.add('z-page');
    endlessScrollDiv.style['display'] = 'block';
    endlessScrollDiv.innerHTML = "Next Page...";
    return endlessScrollDiv
  }

  /**
   * Adds the next page div that triggers infininte scroll functionality and enables the trigger.
   * @private
   */
  waitingNextPage(toolkitThatCanAdd){
    toolkitThatCanAdd.appendHTML(this.__createNextPageDiv());
    this.ifHandler.searchBase("#BookList #EndlessScroll");
    this.ifHandler.enableInfininteScroll();
  }

  /**
   * Changes the "next page" text to the current page headder.
   * @private
   */
  pageHeader(){
    this.ifHandler.changeID();
    this.ifHandler.replaceText("Page " + this.catalog.filter.page + "/" + this.catalog.filter.max_page);
  }

  /**
   * Changes the "next page" text to the "no more pages" text.
   * @private
   */
  pageEnd(){
    this.ifHandler.changeID("NoMorePages");
    this.ifHandler.replaceText("No more pages!");
  }

  /**
   * Disables infininte scroll tirggers.
   * This helps prevent race conditions.
   * @public
   */
  lockoutInfininteScroll(){
    this.ifHandler.disableInfininteScroll();
  }

  /**
   * When the infininte scroll get's triggered.
   * @private
   */
  onInfininteScroll(status){
    if(status){
      this.ifHandler.disableInfininteScroll();
      this.catalog.filter.nextPage();
      if(this.catalog.filter.page > this.catalog.filter.max_page){
        this.pageEnd();
      } else {
        this.pageHeader();
        this.catalog.nextPage();
      }
    }
  }

}
