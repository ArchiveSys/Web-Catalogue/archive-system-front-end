import { Filter_Base } from './Filter_Base.js';
import moodral from './../../moodral.js';

import { aggregator, toolkit } from './../../toolkit/baseToolkit.js';
import {contentToolkitExtention} from './../../toolkit/contentToolkitExtention.js';
import {eventToolkitExtention} from './../../toolkit/eventToolkitExtention.js';
import {styleToolkitExtention} from './../../toolkit/styleToolkitExtention.js';

class tk extends aggregator(toolkit, contentToolkitExtention, eventToolkitExtention, styleToolkitExtention) {};

/**
 * The Filter object that handles changing the filters
 * @extends {Filter_Base}
 */
export class Filter extends Filter_Base {

  /**
   * The constructor.
   * @public
   * @param {Catalog_System} catalog - the parrent catalog object.
   */
  constructor(catalog) {
    super(catalog);
    let self = this;

    /**
     * The options moodral toolkit.
     * @public
     * @type {toolkit}
     */
    this.moodral = new moodral("SearchBoxMoodral");
    this.moodral.addCloseButton(".moodralClose");
    // I don't trust `this`.
    let filterMoodral = this.moodral;
    document.getElementById('openSearchBoxMoodral').addEventListener("click", (e) => filterMoodral.show(), false);

    /**
     * The page & results displayer CSS Selector.
     * @private
     * @type {String}
     */
    this.pnr = "#pagesNresults";

    // set the triggers
    this.setTriggers();
  }

  /**
   * Set's the filter menu triggers.
   * While technically public, in reality nothing should need to access this.
   * @private
   */
  setTriggers() {
    let self = this;
    let filterForum = new tk('.searchbox');
    filterForum.addListener("submit", function(evt) {
      evt.preventDefault();
      self.onFilterSubmit(true);
    });
    filterForum.searchChildren('select');
    filterForum.addListener("change", function() {
      self.onFilterSubmit(false);
    });
  }

  /**
   * Ran whenever a filter update is triggered.
   * Trigger this if your userscript has made changes to any options.
   * @public
   * @param {boolean} close - If it should auto-close the search box.
   */
  onFilterSubmit(close=false) {
    this.setPage(1);
    this.__generatePayload();
    this.catalog.reloadFilter();
    if(close){
      this.moodral.closeMoodral();
    }
  }

  /**
   * Set's the author ID
   * @public
   * @param {number} authorID - the author ID to set.
   */
   setAuthorID(authorID){
     let elm = document.getElementById("st-author");
     elm.value = authorID;
   }

  /**
   * Generate Payload for HTTP requests.
   * @private
   */
  __generatePayload(){
    // New PayLoad;
    let sets = [["STall", "st-main"], ["STauthor","st-author"], ["STgenre","st-genre"], ["STkeyword","st-key"],
    ["NMpublisher","publisher"], ["NMcatagory","category"], ["NMrating","rating"], ["NMstatus","status"],
    ["NMchapMin","min-chap"], ["NMchapMax","max-chap"], ["NMwordMin","min-words"], ["NMwordMax","max-words"]];
    let selectors = []
    for(let id in sets){
      let set = sets[id];
      let v = this.__getValue(set[1]);
      if(v){
        selectors.push({"type":set[0], "value":v});
      }
    }
    if (this.x){
      selectors.push({"type":"reveal"});
    }
    let npl = {selectors};
    npl.sort = this.__getValue("sort");
    npl.offset = this.page;
    this.__payload = npl;
  }

  /**
   * Removes all filter options and re-adds the dummy option.
   * @private
   * @param {String} selectID - The ID of the element.
   * @param {String} dummyString - The string for the dummy item.
   */
  __resetOptions(selectID, dummyString){
    let dummyOption = '<option value="dummy">' + dummyString + ':</option>';
    let v = this.__getValue(selectID);
    let selectElement = new tk("#"+selectID);
    selectElement.replaceHTML(dummyOption);
    return v;
  }

  /**
   * Makes a option node that can be added to a select element.
   * @private
   * @param {Object} optionData - the actual option data.
   * @return {Node}
   */
  __makeOption(optionData){
    let newOption = document.createElement('option');
    // boolean ? true condition : false condition
    let optionName = optionData.OptionAlt ? optionData.OptionAlt : optionData.OptionName;
    let optionText = optionName + " (" + optionData.OptionCount + ")";
    if (optionData.OptionText) {
      optionText = optionData.OptionText;
    }
    newOption.title = optionText;
    newOption.value = optionData.OptionID;
    newOption.innerHTML = optionText;
    return newOption;
  }

  /**
   * Adds all the new options.
   * @private
   * @param {String} selectID - The ID of the element.
   * @param {Array<Object>} newOptions - The ID of the element.
   * @param {String} payloadItem - The payloadValue containing the last selected item.
   */
  __addOptions(selectID, newOptions, payloadItem){
    let selectElement = new tk("#"+selectID);
    let added = 0
    for (let k in newOptions){
      // Some guy on stack exchange said to do this.
      if (newOptions.hasOwnProperty(k)) {
        selectElement.appendHTML(this.__makeOption(newOptions[k]));
        added++;
      }
    }
    payloadItem = payloadItem || "dummy";
    if(added == 0){
      selectElement.appendHTML(this.__makeOption({'OptionText':"No valid options were found.", 'OptionID':"dummy2"}));
      payloadItem = "dummy2";
    }
    selectElement.doEach(function(element, index) {
      element.value = payloadItem;
    })
  }

  /**
   * Should update the filter object with the new objects and re-display them.
   * If the options don't change, it should not update the options.
   * @todo actually implement this.
   */
  updateOptions(newOptions) {
    super.updateOptions(newOptions);
    let results = new tk(this.pnr + " #ResCount").replaceText(this.total_results);
    let pages = new tk(this.pnr + " #PgsCount").replaceText(this.max_page);
    // reset the options again. Just in-case.
    let publisher = this.__resetOptions("publisher","Publisher");
    let category = this.__resetOptions("category","Category");
    let rating = this.__resetOptions("rating","Rating");
    let status = this.__resetOptions("status","Status");
    // add the new options.
    this.__addOptions("publisher", this.publishers, publisher);
    this.__addOptions("category", this.catagorys, category);
    this.__addOptions("rating", this.ratings, rating);
    this.__addOptions("status", this.status, status);
  }
}
