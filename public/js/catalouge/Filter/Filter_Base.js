/**
 * The Options object that is still under heavy development.
 * @experimental this class is still under heavy development.
 */
export class Filter_Base {

  /**
   * The constructor.
   * @public
   * @param {Catalog_System} catalog - the parrent catalog object.
   */
  constructor(catalog) {

    if(!(catalog)){
      throw new Error("Catalog object Required To Be Passed");
    }

    /**
     * The parrent catalog.
     * @type {Catalog_System}
     */
    this.catalog = catalog;

    this.__setMembers();
  }

  /**
   * Sets most of the members for the filters.
   * @private
   */
  __setMembers(){
    /**
     * The last generated payload
     * @private
     */
    this.__payload = {};

    /**
     * The page number.
     * @private
     * @type {number}
     */
    this.page = 1;

    /**
     * The maxamum page number.
     * @private
     * @type {number}
     */
    this.max_page = 1;

    /**
     * The total results of the last search.
     * @private
     * @type {number}
     */
    this.total_results = 1;

    /**
     * List of currently avalible options for
     * @private
     * @type {Array<Object>}
     */
    this.publishers = [];
    /**
     * List of currently avalible options for
     * @private
     * @type {Array<Object>}
     */
    this.catagorys = [];
    /**
     * List of currently avalible options for
     * @private
     * @type {Array<Object>}
     */
    this.ratings = [];
    /**
     * List of currently avalible options for
     * @private
     * @type {Array<Object>}
     */
    this.status = [];
    /**
     * For experimental filters.
     * @public
     * @type boolean
     * @experimental
     */
    this.x = false;
  }

  /**
   * Set's the current page number. (starts from 1)
   * @public
   * @param {number} x - the page number (starts from 1)
   */
  setPage(x){
    if(typeof x == "number"){
      if(x > 0){
        this.page = x;
      }
    }
  }

  /**
   * Incraments the page vairable.
   * @public
   */
  nextPage(){
    this.page ++;
  }

  /**
   * Get's the current page number. (starts from 1)
   * @public
   * @return {number} the page number (starts from 1)
   */
  getPage(){
    return this.page;
  }

  /**
   * Set's the author ID
   * @public
   * @param {number} authorID - the author ID to set.
   * @abstract
   */
  setAuthorID(authorID){
  }

  /**
   * Get's the value of a item.
   * @private
   * @param {String} ElementID - The ID of the element we are looking for.
   * @return {String} The value of said element.
   */
  __getValue(ElementID){
    let elm = document.getElementById(ElementID);
    if (elm == null){
      console.warn(ElementID + " is missing.");
    }
    let val = elm.value;
    switch (val) {
      case 'dummy':
      case 'dummy2':
      case null:
      case "":
      case undefined:
        return undefined;
        break;
      default:
        return val;
    }
  }

  /**
   * Generate Payload for HTTP requests.
   * @private
   * @return {Object}
   * @abstract
   */
  __generatePayload(){
  }

  /**
   * Gets the last generated payload for HTTP requests.
   * @public
   */
  getPayload() {
    this.__payload.offset = this.page;
    return this.__payload;
  }

  /**
   * Should update the filter object with the new options.
   */
  updateOptions(newOptions) {
    this.max_page = newOptions.pages[0].MaxPages;
    this.total_results = newOptions.pages[0].TotalResults;
    this.publishers = newOptions.publishers;
    this.catagorys = newOptions.catagories;
    this.ratings = newOptions.ratings;
    this.status = newOptions.status;
  }
}
