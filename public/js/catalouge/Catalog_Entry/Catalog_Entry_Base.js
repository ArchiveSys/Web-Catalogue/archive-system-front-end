/**
 * This is the base Catalouge Entry object.
 * It handles prosessing all the data from the raw data and has a few other helper functions.
 */
export class Catalog_Entry_Base {
  /**
   * The constructor for the Catalog Entry Base.
   * It sets all the local attributes.
   * @public
   * @param {Object} entryData - Entry Data object containing the property's of the entry.
   * @param {Object} catalogObject - The parrent catalog.
   */
  constructor(entryData, catalogObject){

    /**
     * A Constant representing a empty value.
     * Wouldn't it be nice if null !== false?
     * The random characters are to try and ensure it's never equated accedentally.
     * @public
     * @const
     * @example
     * this.sourceURL === this.EMPTY //;
     * = true // the item was empty;
     * = false // the item has data;
     */
    this.EMPTY = "#$%$# MISSING DATA *&%^%&*";

    /**
     * Contains CSS Selectors for vairious parts of the code.
     * Overridden with child classes
     * @protected
     * @type {Enum<String>}
     * @abstract
     */
    this.css_selectors = {};

    /**
     * The parrent catalouge object.
     * @public
     * @type {Object}
     */
    this.catalog = catalogObject;

    this.__parseEntry(entryData);

  }

  /**
   * Calls other parsers to parse the data.
   * Parses the raw entry and saves the data in pre-specified locations.
   * @private
   * @param {Object} RawData - The raw data that is being inputed.
   */
  __parseEntry( RawData ) {
    this.__parseEntryAlways(RawData);
    this.__parseEntrySometimes(RawData);
  }

  /**
   * Parser for values that should always exsist.
   * Parses the raw entry and saves the data in pre-specified locations.
   * @private
   * @param {Object} RawData - The raw data that is being inputed.
   */
  __parseEntryAlways( RawData ) {
    /**
     * The entry's catalouge ID.
     * @public
     * @type {number}
     */
    this.id = RawData["IDC"];

    /**
     * The title of the entry.
     * @public
     * @type {string}
     */
    this.title = RawData["Title"];

    /**
     * The author of the entry.
     * @public
     * @type {{id: number, name:string}}
     */
    this.author = {
      "id": RawData["AuthorID"],
      "name": RawData["Author"]
    };

    /**
     * The publisher of the entry. (Where we got the file from.)
     * @public
     * @type {string}
     */
    this.publisher = RawData["Publisher"];

    /**
     * The blurb / summary for the entry.
     * @public
     * @type {string|DOMString}
     */
    this.blurb = RawData["Summary"];

    /**
     * The catagory for the entry.
     * @public
     * @type {string}
     */
    this.catagories = RawData["Catagory"];

    /**
     * The rating of the entry.
     * @public
     * @type {{short:string, long:string}}
     */
    this.rating = {
      "short": RawData["Rating"],
      "long": RawData["RaitingDef"]
    };

    /**
     * The status of the entry.
     * @public
     * @todo figure out what type this should be.
     */
    this.status = RawData["Complete"];

  }

  /**
   * Parser for values that sometimes exsist.
   * Parses the raw entry and saves the data in pre-specified locations.
   * @private
   * @param {Object} RawData - The raw data that is being inputed.
   */
  __parseEntrySometimes( RawData ) {
    /**
     * The dates related to the entry.
     * @public
     * @type {{Published:date, Added:date|EMPTY, Downloaded:date|EMPTY, Checked:date|EMPTY, Updated:date|EMPTY}}
     */
    this.dates = {
      "Published": this.__hasData(RawData["Published"]),
      "Added": this.__hasData(RawData["Downloaded"]),
      "Downloaded": this.__hasData(RawData["Downloaded"]),
      "Checked": this.__hasData(RawData["Loaded"]),
      "Updated": this.__hasData(RawData["Updated"])
    };

    /**
     * The count of varius stuff.
     * Chapters, Words, Minutes, Pages, Etc.
     * @public
     * @type {{chapters:number|EMPTY, words:number|EMPTY}}
     */
    this.count = {
      "chapters": this.__hasData(RawData["ChapterCount"]),
      "words": this.__hasData(RawData["WordCount"])
    };
    // REVIEW: This is not currently used.
    //         Should be saved on a moodral instead of on the main list.
    /**
     * @TODO Write details on this when it's implemented.
     *
     */
    this.times = {
      "LastCheck": this.__hasData(RawData["LoadedInt"]),
      "NextUpdate": this.__hasData(RawData["TimeToUpdate"])
    };

    // TEMP: Keywords should be phased out and replaced with collections & series tags.
    /**
     * Keywords related to the entry
     * @public
     * @type {string|EMPTY}
     * @todo This will be phased out in favor of collections, series's and tags.
     */
    this.keywords = this.__hasData(RawData["KeyWords"]);

    /**
     * If the entry is normally hidden.
     * @public
     * @type {boolean|EMPTY}
     */
    this.hidden = this.__hasData(RawData["LockedCombined"]);

    /**
     * Genres of the entry.
     * @public
     * @type {string|EMPTY}
     */
    this.genres = this.__hasData(RawData["genres"]);

    /**
     * The Source URL for the entry.
     * @public
     * @type {string|EMPTY}
     */
    this.sourceURL = this.__hasData(RawData["URL"]);

    /**
     * Major Characters in the entry.
     * @public
     * @type {string|EMPTY}
     */
    this.characters = this.__hasData(RawData["characters"]);

    /**
     * Character Pairings in the entry.
     * @public
     * @type {string|EMPTY}
     */
    this.pairings = this.__hasData(RawData["pairings"]);

  }

  /**
   * Checks if input actually has data.
   *
   * Checks if data has data, if not, returns the const EMPTY.
   * @private
   * @param {*} data - The stuff we are checking.
   * @param {boolean} legacy - Legacy option that fixes some bad data.
   * @todo remove legacy option.
   * @return {*|EMPTY} if data is empty, returns EMPTY Const, otherwise returns inputed data.
   *
   */
  __hasData(data, legacy){
    // OPTIMIZE: Figure out what the spread on this is and make it a tree of if statements.
    //           If done right every empty item should be sent as a undefined,
    //           allowing us to skip all other checks.
    legacy = legacy || false;
    switch (data) {
      case "":
      case null:
      case undefined:
        return this.EMPTY;
        break;
      case 0:
      case "0":
        // TEMP: Remove legacy stuff when it is phased out.
        if(legacy){
          return this.EMPTY;
          break;
        }
      default:
        return data;
    }
  }



  /**
   * The code that is triggered when the author name is clicked.
   * @private
   */
  __transferAuthorToSearch(){
    this.catalog.filter.setAuthorID(this.author.id);
    this.catalog.filter.onFilterSubmit();
  }

  /**
   * Converts a unix time duration to text for days, hours, minutes, seconds.
   * @public
   * @param {number} timeDuration - the time duration to prosess.
   * @return {String}
   */
  unixToReadable(timeDuration) {
    let s = "";
    if (timeDuration < 0) {
      s += "Negative ";
      timeDuration = Math.abs(timeDuration);
    }
    let numDays = Math.floor(timeDuration / 86400);
    if(numDays > 0){
      s += (numDays + " days ");
    }
    let numHours = Math.floor((timeDuration % 86400) / 3600);
    if(numHours > 0){
      s += (numHours + " hours ");
    }
    let numMinutes = Math.floor(((timeDuration % 86400) % 3600) / 60);
    if(numMinutes > 0){
      s += (numMinutes + " minutes ");
    }
    let numSeconds = Math.round(((timeDuration % 86400) % 3600) % 60);
    if(numSeconds > 0){
      s += (numSeconds + " seconds");
    }
    return s;
  }

  /**
   * Removes trailing sets of zero's from date-times.
   *
   * 2018-10-28 19:55:36 -> 2018-10-28 19:55:36
   * 2018-10-28 19:55:00 -> 2018-10-28 19:55
   * 2018-10-28 00:00:00 -> 2018-10-28
   * @public
   * @param {dateTime|String} dateTime - The date-time to prosess.
   * @returns {String}
   */
  truncateZeroDateTimes(dateTime) {
    return dateTime.toString().replace(/(\s{0,1}:{0,1}00){1,3}/g, "");
  }

  /**
   * Adds commas to numbers in the thousands, millions, etc places.
   * @public
   * @param {number|String} number - The number to add the commas to.
   * @return {String}
   */
  addCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
