import { Catalog_Entry_Base as CEB } from './Catalog_Entry_Base.js';

import { aggregator, toolkit } from './../../toolkit/baseToolkit.js';
import { eventToolkitExtention } from './../../toolkit/eventToolkitExtention.js'

/**
 * The toolkit used throughout this file
 */

class tk extends aggregator(toolkit, eventToolkitExtention) {};

/**
 * This is the Catalog Entry Displayer.
 * This wraps the Catalog Entry Base and handles generateing all the content
 * @todo Should be renamed to being a more generic name.
 * @extends {Catalog_Entry_Base}
 */
export class Catalog_Entry extends CEB {
  /**
   * The constructor for books.
   * It sets all the local attributes.
   * @public
   * @param {Object} bookData - Book Data object containing the property's of the book.
   * @param {Object} catalougeObject - The parrent catalogue.
   */
  constructor(bookData, catalogObject, HTTP_Entrypoint){

    super(bookData, catalogObject);

    /**
     * The base HTTP Entrypoint
     * @type {String}
     */
    this.HTTP_Entrypoint = HTTP_Entrypoint;

    /**
     * This helps with looking for the catalouge entry.
     * @private
     * @type {{raw: string, hashed: string}}
     */
    let __domID = {
      'RAW': 'CI_' + this.id,
      'HASHED': '#CI_' + this.id
    };

    /**
     * Contains CSS Selectors for vairious parts of the code.
     * @protected
     * @type {Enum<String>}
     */
    this.css_selectors = {'AUTHOR_TEXT': __domID.HASHED + ' .c-author', 'DOM_ID': __domID};

  }

  /**
   * Generates HTML of the catalouge entry.
   *
   * Every sub-vairable should be a seperate function for future-proofing.
   * @protected
   * @return {Node} The Node with the entry.
   */
  __generateHTML() {
    let html = "";
    {
      {
        html += this.__image();
        html += '<div class="c-top">';
        {
          html += '<div class="c-line">';
          {
            html += this.__title();
            html += this.__author();
            html += this.__publisher();
            html += '<span>☆</span>';
          }
          html += '</div>';
          html += '<div class="c-line">';
          {
            html += this.__catagory();
          }
          html += '</div>';
          html += '<div class="c-line">';
          {
            html += this.__rating();
            html += this.__status();
          }
          html += '</div>';
          html += '<div class="c-line">';
          {
            html += this.__chapters();
            html += this.__wordCount();
          }
          html += '</div>';
        }
        html += '</div>';
        html += '<div class="c-main">';
        {
          html += '<blockquote class="c-blurb">' + this.blurb + '</blockquote>';
          html += '<div class="c-facts">';
          {
            html += this.__genres();
            html += this.__published();
            html += this.__updated();
            html += this.__downloaded();
            html += this.__checked();
            html += this.__characters();
            html += this.__pairings();

          }
          html += '</div>';
        }
        html += '</div>';
      }
    }

    let wrappingDiv = document.createElement('div');
    wrappingDiv.id = this.css_selectors.DOM_ID.RAW;
    wrappingDiv.classList.add('z-list');
    if(this.hidden >= 1){
      wrappingDiv.classList.add('hidden');
    }
    wrappingDiv.style['display'] = 'block';
    wrappingDiv.innerHTML = html

    return wrappingDiv;
  }

  /**
   * Adds the HTML of the catalouge entry to the DOM useing a displayerObject.
   *
   * This actually just calls __generateHTML() and then appends the returned HTML.
   * @public
   * @param {toolkit} displayerObject - Toolkit displayer object that can append HTML.
   */
  addHTML(displayerObject){
    let html = this.__generateHTML();
    displayerObject.appendHTML(html);
  }

  /**
   * Adds event listeners to the catalouge entry.
   *
   * Should only be called after the html is added.
   * @public
   */
  addEventListeners(){
    let __self = this;
    let authorTK = new tk(this.css_selectors.AUTHOR_TEXT)
    authorTK.addListener('click', (e) => {__self.__transferAuthorToSearch();})
  }

  /**
   * Code to add this entry to the DOM.
   *
   * Adds the entry to the dom and sets event triggers
   * @public
   * @param {toolkit} displayerObject - Toolkit displayer object that can append HTML.
   */
  addEntry(displayerObject){
    this.addHTML(displayerObject);
    this.addEventListeners();
  }

  /* Reduces redundancy in content displayers */

  /**
   * Generates DOMString of lower metadata info.
   *
   * This is to reduce redundancy of the lower functions and to reduce probability of bugs.
   * @private
   * @param {string} start - What the info is about
   * @param {string} tagClass - What class the css class should be
   * @param {string} content - what the actual value should be
   * @param {string|boolean} [title=false] - what the hover text should be
   * @param {Object|boolean} [data=false] - ! IGNORED !
   * @param {boolean} [tagStart=false] - ! IGNORED !
   * @return {DOMString}
   * @example this.__lowerInfo("Published", "published", "02/19/2018 - 12", "02/19/2018 - 12:00:00");
   * @todo make this one work just like the upper info one. (Also switch them around in the object source)
   */
  __lowerInfo(start, tagClass, content, title, data, tagStart){
    let t = "";
    // the start and class
    t += '<span class="c-bufferWidth">' + start + ':</span> <span class="c-' + tagClass + '"';
    // the title if needed
    if (title) {
      t += ' title="' + title + '"';
    }
    // closeing it out and the content
    t += '>' + content + '</span>';
    return t;
  }

  /**
   * Generates DOMString of upper metadata info.
   *
   * This is to reduce redundancy of the upper functions and to reduce probability of bugs.
   * @private
   * @param {string} start - What the info is about
   * @param {string} tagClass - What class the css class should be
   * @param {string} content - what the actual value should be
   * @param {string|boolean} [title=false] - what the hover text should be
   * @param {Object|boolean} [data=false] - data to add to the tag
   * @param {boolean} [tagStart=false] - if a dash should be added infront of the tag
   * @return {DOMString}
   * @example this.__lowerInfo("By", "author", "Jcc10", "ID: 01", [{'title':'authorID', 'content':'01'}], true);
   */
  __topInfo(start, tagClass, content, title, data, tagStart){
    let t = "";
    // the dash at the start of the tag if requested
    if (tagStart) {
      t += ' - ';
    }
    // the start text and the class
    t += '' + start + ': <span class="c-' + tagClass + '"';
    // the title if needed
    if (title) {
      t += ' title="' + title + '"';
    }
    // the data if any exsists
    if (data) {
      // the actual looping function
      for(let dataItemIndex in data){
        t += ' data-' + data[dataItemIndex].title + '="' + data[dataItemIndex].content + '"';
      }
    }
    // closeing it out and the content
    t += '>' + content + '</span>';
    return t;
  }

  /**
   * Wraps content in a line div.
   * @private
   * @param {DOMString} content
   * @returns {DOMString}
   */
  __wrapInLine(content){
    return ('<div class="c-line">' + content + '</div>');
  }

 /* Individual content displayer's */

  /**
   * Generates the cover image tags
   * @private
   * @returns {DOMString}
   * @todo implement actual cover system.
   * @see https://www.google.com/search?q=base64+image+encoder&ie=utf-8&oe=utf-8
   */
  __image(){
   let defaultURL = this.HTTP_Entrypoint + 'images/d_60_90.jpg';
   return '<img class="c-image" width="50" height="66" src="' + defaultURL + '">';
  }
  /**
   * Generates the link to the reader.
   * @private
   * @returns {DOMString}
   */
  __link(){
    return this.HTTP_Entrypoint + 'reader.php?id=' + this.id;
  }
  /**
   * Generates the title tags.
   * @private
   * @returns {DOMString}
   */
  __title(){
    return '<a class="c-title" href="' + this.__link() + '" target="_blank">' + this.title + '</a>';
  }
  /**
   * Generates the author tags.
   * @private
   * @returns {DOMString}
   */
  __author(){
    let c = ('<b>' + this.author.name + '</b>');
    let t = ("ID: " + this.author.id);
    let d = [{'title':'authorID','content':this.author.id}];
    return this.__topInfo('By', 'author', c, t, d, true);
  }
  /**
   * Generates the publisher tags.
   * @private
   * @returns {DOMString}
   */
  __publisher(){
    if (this.sourceURL === this.EMPTY){
      return ' <a class="c-publisher">' + this.publisher + '</a> &nbsp;';
    } else {
      return ' <a class="c-publisher" href="' + this.sourceURL + '" target="_blank">' + this.publisher + '</a> &nbsp;';
    }
  }
  /**
   * Generates the catagory tags.
   * @private
   * @returns {DOMString}
   */
  __catagory(){
    return this.__topInfo('Catagory', 'catagory', this.catagories);
  }
  /**
   * Generates the rating tags.
   * @private
   * @returns {DOMString}
   */
  __rating(){
    return this.__topInfo('Rating', 'rating', this.rating.short, this.rating.long);
  }
  /**
   * Generates the status tags.
   * @private
   * @returns {DOMString}
   * @todo re-work this when the new status get's implemented
   */
  __status(){
    if (this.status == 1) {
      return this.__topInfo('Status', 'status', 'Complete', false, false, true);
    } else if (this.status == 0) {
      return this.__topInfo('Status', 'status', 'In-Progress', false, false, true);
    }
    return this.__topInfo('Status', 'status', this.status, false, false, true);
  }
  /**
   * Generates the chapters tags.
   * @private
   * @returns {DOMString}
   */
  __chapters(){
    if (this.count.chapters === this.EMPTY){
      return "";
    }
    return this.__topInfo('Chapters', 'chapters', this.addCommas(this.count.chapters));
  }
  /**
   * Generates the word count tags.
   * @private
   * @returns {DOMString}
   */
  __wordCount(){
    if (this.count.words === this.EMPTY){
      return "";
    }
    return this.__topInfo('Word Count', 'words', this.addCommas(this.count.words), false, false, true);
  }
  /**
   * Generates the genre(s) tags.
   * @private
   * @returns {DOMString}
   */
  __genres(){
    if (this.genres === this.EMPTY){
      return "";
    }
    let c = this.__lowerInfo('Genres', 'genres', this.genres);
    return (this.__wrapInLine(c));
  }
  /**
   * Generates the publish date tags.
   * @private
   * @returns {DOMString}
   */
  __published(){
    let c = ""
    if(this.dates.Published === this.EMPTY){
      c = this.__lowerInfo('Published', 'datePublished', 'Unknown')
    } else {
      c = this.__lowerInfo('Published', 'datePublished', this.truncateZeroDateTimes(this.dates.Published), this.dates.Published);
    }

    return (this.__wrapInLine(c));
  }
  /**
   * Generates the updated date tags.
   * @private
   * @returns {DOMString}
   */
  __updated(){
    if (this.dates.Updated === this.EMPTY){
      return "";
    }
    let c = this.__lowerInfo('Updated', 'dateUpdated', this.truncateZeroDateTimes(this.dates.Updated), this.dates.Updated);
    return (this.__wrapInLine(c));
  }
  /**
   * Generates the downloaded date tags.
   * If a book has not yet been downloaded it will not have this tag.
   * @todo remove the dummy return when the database upgrade is completed.
   * @private
   * @returns {DOMString}
   */
  __downloaded(){
    // TEMP: this function is not actually being used right now.
    return "";
    let c = "";
    if (this.dates.Downloaded === this.EMPTY){
      c = this.__lowerInfo('Last Downloaded', 'dateDownloaded', 'Not Downloaded Yet');
    } else {
      c = this.__lowerInfo('Last Downloaded', 'dateDownloaded', this.truncateZeroDateTimes(this.dates.Downloaded), this.dates.Downloaded);
    }
    return (this.__wrapInLine(c));
  }
  /**
   * Generates the checked date tags.
   * @private
   * @returns {DOMString}
   */
  __checked(){
    let c = ""
    if(this.dates.Published === this.EMPTY){
      c = this.__lowerInfo('Last Check', 'dateChecked', 'Unknown')
    } else {
      c = this.__lowerInfo('Last Check', 'dateChecked', this.truncateZeroDateTimes(this.dates.Checked), this.dates.Checked);
    }

    return (this.__wrapInLine(c));
  }
  /**
   * Generates the characters tags.
   * @private
   * @returns {DOMString}
   */
  __characters(){
    if (this.characters === this.EMPTY){
      return "";
    }
    let c = this.__lowerInfo('Characters', 'characters', this.characters);
    return (this.__wrapInLine(c));
  }
  /**
   * Generates the pairings tags.
   * @private
   * @returns {DOMString}
   */
  __pairings(){
    if (this.pairings === this.EMPTY){
      return "";
    }
    let c = this.__lowerInfo('Pairings', 'pairings', this.pairings);
    return (this.__wrapInLine(c));
  }
}
